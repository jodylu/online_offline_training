import csv

from rpy2 import robjects
from rpy2.robjects import packages

nn = packages.importr('neuralnet')
base = packages.importr('base')

def train(dataFrame):
    """
    dataset: R data frame
    """
    formula = 'default10yr ~ LTI + age'
    model = nn.neuralnet(formula, dataFrame, hidden=4, lifesign='minimal',
                         linear_output=False, threshold=0.1)
    return model

def convertToDataFrame(datadict):
    """
    data: dictionary
    """
    dataFrame = robjects.DataFrame(datadict)
    return dataFrame


if __name__ == '__main__':
    datadict = {'clientid':[], 'income':[], 'age':[], 'loan':[], 'LTI':[], 'default10yr':[]}
    dataset = []

    with open('creditset.csv') as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            dataset.append(row)
    
    for data in dataset:
        for key in data:
            datadict[key].append(data[key])

    datadict['clientid'] = robjects.IntVector(datadict['clientid'])
    datadict['income'] = robjects.FloatVector(datadict['income'])
    datadict['age'] = robjects.FloatVector(datadict['age'])
    datadict['loan'] = robjects.FloatVector(datadict['loan'])
    datadict['LTI'] = robjects.FloatVector(datadict['LTI'])
    datadict['default10yr'] = robjects.IntVector(datadict['default10yr'])

    dataFrame = convertToDataFrame(datadict)
    model = train(dataFrame)
    tmp_test = base.subset(dataFrame, select=base.c('LTI', 'age'))
    res = nn.compute(model, tmp_test)
    res = dict(zip(res.names, list(res)))
    print(res.keys())
    print(res['net.result'])
