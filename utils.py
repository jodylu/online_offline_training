import requests
import ast
#import riak
import numpy as np
import sys
import os

from rpy2 import robjects
from rpy2.robjects import packages
from contextlib import contextmanager

from influxdb import InfluxDBClient
from config import *


def queryRiakTS(riak_server, table, begin, end):
    """
    Query to riakTS server 
    @riak_server: ip address of riak server
    @table: table to be quried
    @begin: begin time
    @end: end time
    """
    data = {i:[] for i in range(n_class)}
    client = riak.RiakClient(host=riak_server, http_port=8098)
    query = "select * from {p1} where time>{p2} and time<{p3} and label={p4}"

    for i in range(n_class):
        q = query.format(p1=table, p2=begin, p3=end, p4=i)
        response = client.ts_query(table, q)
        for row in response.rows:
            gearSpeed = row[2]
            label = row[3]
            data[label].append(gearSpeed)
    
    return data



def queryGraphite(graphite_server, tags, begin, end):
    """
    Query to graphite server
    @tags: metric's tag to query
    @begin: begin timestamp
    @end: end timestamp
    """
    data = {i:[] for i in range(n_class)}
    url = graphite_server + '/events/get_data?tags={}&from={}&until={}'.format(tags, begin, end)
    response = requests.get(url)
    json = response.json()
    
    for obj in json:
        for row in ast.literal_eval(obj['data']):
            load = float(row[2])
            label = int(row[4])
            data[label].append(load)

    return data


def queryInfluxDB(influxdb_server, metric, table, db, tags=None, begin=0, end=0):
    """
    Query to influxDB server
    @influxdb_server: ip address of influxdb server
    @metric: name of measurement
    @db: name of database
    """
    data = {i:[] for i in range(n_class)}
    client = InfluxDBClient(influxdb_server, influxdb_port, 'root', 'root', db)
    
    if tags is None:
        query = 'select {} from {}'.format(metric, table)
    else:
        query = 'select {} from {} where time <= now() and time >= {}'.format(metric, table, begin)

    result = client.query(query)
    
    if tags is None:
        for row in result.get_points():
            data[row['label']].append(row['gearSpeed'])
    
    return data


def processData(data):
    """
    Process data into neuralnet input format 
    @data: {label:list of data points}
    """
    newData = {'label': []}
    labels = ['y{}'.format(i) for i in range(n_class)]

    for label in labels:
        newData[label] = []

    for i in range(window_size):
        key = 'x{}'.format(i) 
        newData[key] = []
    
    for key in data:
        length = len(data[key])
        length -= (length % window_size)
        data[key] = data[key][:length]
        data[key] = np.array(data[key])
        size = int(length/window_size)
        data[key] = data[key].reshape((size, window_size))
    
    for key in data:
        for row in data[key]:
            newData['label'].append(key)
            
            for i, label in enumerate(labels):
                newData[label].append(int(key == i))
            
            for i, p in enumerate(row):
                k = 'x{}'.format(i)
                newData[k].append(p)
    
    return newData
        

def convertToDataFrame(data):
    """
    Convert dict to R data frame
    @data: data from processData
    """
    data['label'] = robjects.IntVector(data['label'])
    labels = ['y{}'.format(i) for i in range(n_class)]

    for label in labels:
        data[label] = robjects.IntVector(data[label])
    
    for i in range(window_size):
        key = 'x{}'.format(i)
        data[key] = robjects.FloatVector(data[key])

    dataFrame = robjects.DataFrame(data)
    return dataFrame


def merge_dicts(dict1, dict2):
    """
    Merge two dictionary of lists
    @dict1: {key:list}
    """
    keys = set(dict1).union(dict2)
    no = []
    res = dict((k, list(dict1.get(k, no)) + list(dict2.get(k, no))) for k in keys)
    return res

def get_model_weight(model):
    """
    Get weights from model
    @model: R neuralnet model
    """
    model = dict(zip(model.names, list(model)))
    weight = model['weights']
    return weight


def predict(model, dataFrame, data):
    """
    Predict the accuracy based on model
    @model: R neuralnet model
    @dataFrame: input data frame
    @data: input data
    """
    features = ['x{}'.format(i) for i in range(window_size)]
    tmp_test = base.subset(dataFrame, select=base.c(*features))
    res = nn.compute(model, tmp_test)
    res = dict(zip(res.names, list(res)))
    res = np.array(res['net.result'])
    res = np.argmax(res, axis=1)
    train_y = data['label']
    print("Accuracy:", float(np.sum(res == train_y)) / len(res))


def train(dataset, weight=None):
    """
    Train R neuralnet model
    @dataset: R data frame
    @weight: Initial weight for neuralnet model
    """
    features = ['x{}'.format(i) for i in range(window_size)]
    labels = ['y{}'.format(i) for i in range(n_class)]
    formula = '{} ~ {}'.format(' + '.join(labels), ' + '.join(features))
    if weight is None:
        model = nn.neuralnet(formula, dataset, hidden=10,
                            lifesign='minimal', linear_output=False, threshold=0.1)
    else:
        model = nn.neuralnet(formula, dataset, hidden=10, startweights=weight, 
                             lifesign='minimal', linear_output=False, threshold=0.1)
    return model

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout


if __name__ == '__main__':
    """Graphite
    graphite_server = 'http://192.168.0.228:80'
    data = queryGraphite(graphite_server, 'load', 1522947591, 'now')
    data = processData(data)
    dataFrame = convertToDataFrame(data)
    print(dataFrame)
    """
    #InfluxDB
    influxdb_server = '192.168.0.249'
    db = 'TestBearingData'
    data = queryInfluxDB(influxdb_server, '*', 'Offline', db, begin=100)
    data = processData(data)
    dataFrame = convertToDataFrame(data)
    print(dataFrame)
    
    """
    riak_server = '192.168.0.214'
    table = 'BearingMachineDatav1'
    begin = 1519078166
    end = 1519079948
    data = queryRiakTS(riak_server, table, begin, end)
    data = processData(data)
    dataFrame = convertToDataFrame(data)
    print(dataFrame)
    """
