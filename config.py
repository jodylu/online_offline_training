from rpy2.robjects import packages

## TSDB servers
graphite_server = 'http://192.168.0.240:8086'
influxdb_server = '192.168.0.249'
riak_server = '192.168.0.214'

## KV-store server
redis_server = '192.168.0.240'
redis_port = 6379

## Configuration variables
wait_time = 1
window_size = 5
n_class = 4
influxdb_port = 8086

## R packages
httr = packages.importr('httr')
base = packages.importr('base')
nn = packages.importr('neuralnet')
mgcv = packages.importr('mgcv')
rredis = packages.importr('rredis')
