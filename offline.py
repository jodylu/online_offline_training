from rpy2.robjects import packages
from utils import *
from config import *

import argparse
import logging
import redis


def main():
    parser = argparse.ArgumentParser(description='Offline training.')
    parser.add_argument('db_type', metavar='db', type=str,
                        help='databse type will be quried')
    args = parser.parse_args()
    #redis_client = redis.Redis(host=redis_server, port=redis_port)
    rredis.redisConnect(redis_server, redis_port)
    rredis.redisDelete('offline_begin')
    rredis.redisDelete('offline_end')

    while not rredis.redisGet('offline_end'):
        continue
    
    if args.db_type == 'Graphite':
        begin = int(int(rredis.redisGet('offline_begin')[0]) / 10 ** 9)
        end = int(int(rredis.redisGet('offline_end')[0]) / 10 ** 9)#'now'
        print(begin, end)
        tags = 'load'
        data = queryGraphite(graphite_server, tags, str(begin), str(end))
    elif args.db_type == 'InfluxDB':
        db = 'TestBearingData'
        table = 'Offline'
        data = queryInfluxDB(influxdb_server, '*', table, db)
    elif args.db_type == 'Riak':
        table = 'BearingMachineDatav1'
        begin = 1519078166
        end = 1519079948
        data = queryRiakTS(riak_server, table, begin, end)
    else:
        logging.info("DB {} has not implemented yet".format(args.db_type))
        return

    data = processData(data)
    dataFrame = convertToDataFrame(data)
    with suppress_stdout():
        model = train(dataFrame)
    predict(model, dataFrame, data)
    base.saveRDS(model, file='model.rds')

if __name__ == '__main__':
    main()
