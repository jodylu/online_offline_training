from rpy2.robjects import packages
from config import *
from utils import *

import os.path
import time
import argparse

class OnlineTraining:
    def __init__(self, server, timestamps=None, model='model.rds', db='Graphite'):
        rredis.redisConnect(redis_server, redis_port)
        rredis.redisDelete('online_begin')
       
        while not rredis.redisGet('online_begin') or not os.path.isfile(model):
            continue

        self.server = server
        self.pre_model = model
        self.lastTime = int(int(rredis.redisGet('online_begin')[0]) / 10 ** 9)
        self.max_iteration = 10000
        self.total_data = {i:[] for i in range(n_class)}
        self.threshold = 100
        self.db_type = db

        if self.db_type == 'Graphite':
            self.tags = 'load'
        elif self.db_type == 'InfluxDB':
            self.table = 'Offline'
            self.db = 'TestBearingData'
        elif self.db_type == 'Riak':
            self.table = 'TestBearingData'


    def run(self):
        model = base.readRDS(self.pre_model)
        labels = ['y{}'.format(i) for i in range(n_class)]
        features = ['x{}'.format(i) for i in range(window_size)]
        formula = '{} ~ {}'.format(' + '.join(labels), ' + '.join(features))
        remained = False

        for i in range(self.max_iteration):
            currTime = int(time.time())
            if not remained:
                data = self.query_by_database(self.lastTime, currTime)
                self.total_data = merge_dicts(self.total_data, data)
                print(data)
            
            if self.is_valid():
                remained = True
                processed_data = processData(self.total_data)
                dataFrame = convertToDataFrame(processed_data)
                predict(model, dataFrame, processed_data)
                weight = get_model_weight(model)
                with suppress_stdout():
                    model = train(dataFrame, weight)
                self.resize_data()
            else:
                print("Wait...")
                remained = False
                #self.lastTime = currTime
                time.sleep(wait_time)
            

    def is_valid(self):
        for key in self.total_data:
            if len(self.total_data[key]) < self.threshold:
                return False
        return True


    def resize_data(self):
        for key in self.total_data:
            self.total_data[key] = self.total_data[key][self.threshold + 1:]

    def query_by_database(self, begin, end):
        if self.db_type == 'Graphite':
            return queryGraphite(graphite_server, self.tags, begin, end)
        elif self.db_type == 'InfluxDB':
            return queryInfluxDB(influxdb_server, '*', self.table, self.db, begin=begin, end=end)
        elif self.db_type == 'Riak':
            return queryRiakTS(riak_server, self.table, begin, end)
        else:
            return None

def main():
    server = None
    database = ''
    parser = argparse.ArgumentParser(description='Online training.')
    parser.add_argument('db_type', metavar='db', type=str,
                        help='databse type will be quried')
    args = parser.parse_args()
    

    if args.db_type == 'Graphite':
        server = graphite_server
    elif args.db_type == 'InfluxDB':
        server = influxdb_server
    elif args.db_type == 'Riak':
        server = riak_server
    else:
        return
    
    database = args.db_type
    online_train = OnlineTraining(server, db=database)
    online_train.run()

if __name__ == '__main__':
    main()
